// File: tmdb_api.js
var movie_cell_template = "<div class='col-sm-3 movie-cell'><div class='movie-poster'><img></div><div class='movie-title'></div></div>";
var search_term = $('.input');
var search_base_url = "https://api.themoviedb.org/3/search/movie?";
var poster_base_url = "https://image.tmdb.org/t/p/w500"
var search_api_url = getSearchUrl(search_term);   // use this string to build your search from search_url and search_term

// use this function to concantenate (build) your search url
function getSearchUrl(queryString) {
  var url = search_base_url + "api_key=efe2fb84dbb7ab686e3549d9eae2e9ea&language=en-US&query=" + queryString;
  return url;
}

// use this function to concantenate (build) your poster url
function getPosterUrl(imageString) {
  var url = poster_base_url + imageString;
  return url;
}

// Shorthand for $( document ).ready()
$(function() {
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 1: key input processing
  // ========================================================
  $('input').on({
    // keydown - backspace or delete processing only
    keydown: function(e) {
      var code = e.which;
      console.log('KEYDOWN-key:' + code);
    },
    // keypress - everything else processing
    keypress: function(e) {
      var code = e.which;
      console.log('KEYPRESS-key:' + code);
    }
  });
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 2:  process search on omdb
  // ========================================================
  $('#get-movie-btn').on('click', api_search);

  // Do the actual search in this function
  function api_search(e) {
    // prepare search string url => search_api_url
    $.ajax({
      url: search_api_url,
      dataType: "jsonp",
      success: function(data) {
        console.log(search_api_url);
        render(data);
      },
      cache: false
    });
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 3: perform all movie grid rendering operations
  // ========================================================
  function render(data) {
    var title = data['results']['title'];
    var poster = data['results']['poster-path'];
    // get movie data from search results data

    // select the movie grid and dump the sample html (if any)

    // every group of 4 gets their own class=row
    if((i % 4) === 0) {  
      // enter a new <div class="row">
     var div = document.getElementById('row');
     document.body.appendChild(div);
    }
    $(rowselector).append(movie_cell_template);
    // utilize the movie_cell_template to append, then add data from
    // movie data results (parsed JSON)
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
});